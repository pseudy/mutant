var appControllers = angular.module('appControllers', []);

appControllers.controller('indexCtrl', function ($scope, Data){
  $scope.load = function (name) {
    if (!name) {
      Data.toast({ status: "error", message: "Fyll i ett namn för att ladda"} );
    } else {
      Data.post('get_mutant_character', {
          name: name
      }).then(function (data) {
          if(data.length) {
            angular.forEach(data[0], function(value, key) {
              if(key in $scope.char) {
                if (!isNaN(parseInt(value, 10))) {
                  $scope.char[key] = parseInt(value, 10);
                } else {
                  $scope.char[key] = value;
                }
              }
            });
            $scope.char.displayname = data[0].displayname;
          } else {
            Data.toast({ status: "error", message: "Kunde inte hitta någon karaktär med det namnet"} );
          }
      });
    }
  }
  $scope.update = function (data) {
    var obj = {};
    if (!data || !data.displayname) {
      Data.toast({ status: "error", message: "Fyll i ett namn för att spara"} );
    } else {
      obj = $scope.char;
      obj.displayname = data.displayname;
      angular.forEach($scope.charForm, function(value, key) {
        if(!key in $scope.char || key[0] == '$' || value.$pristine) return;
        obj[value.$name] = data[value.$name];
      });

      Data.post('mutant_character', {
          char: obj
      }).then(function (results) {
          Data.toast(results);
      });
    }
  }
  $scope.initForm = function() {
    $scope.char = {
      basic_0: 0,
      basic_0_0: 0,
      basic_0_1: 0,
      basic_0_2: 0,
      basic_0_3: 0,
      basic_0_4: 0,
      basic_1: 0,
      basic_1_0: 0,
      basic_1_1: 0,
      basic_1_2: 0,
      basic_1_3: 0,
      basic_1_4: 0,
      basic_2: 0,
      basic_2_0: 0,
      basic_2_1: 0,
      basic_2_2: 0,
      basic_2_3: 0,
      basic_2_4: 0,
      basic_3: 0,
      basic_3_0: 0,
      basic_3_1: 0,
      basic_3_2: 0,
      basic_3_3: 0,
      basic_3_4: 0,
      state_0_0: 0,
      state_0_1: 0,
      state_1_0: 0,
      state_1_1: 0,
      state_0: "",
      state_1: "",
      skill_0: 0,
      skill_1: 0,
      skill_2: 0,
      skill_3: 0,
      skill_4: 0,
      skill_5: 0,
      skill_6: 0,
      skill_7: 0,
      skill_8: 0,
      skill_9: 0,
      skill_10: 0,
      skill_11: 0,
      skill_free_0: "",
      skill_12: 0,
      skill_free_1: "",
      skill_13: 0,
      exp_0: 0,
      exp_1: 0,
      exp_2: 0,
      exp_3: 0,
      exp_4: 0,
      exp_5: 0,
      exp_6: 0,
      exp_7: 0,
      exp_8: 0,
      exp_9: 0,
      face: "",
      body: "",
      clothes: "",
      mutation_0: "",
      mutation_1: "",
      mutation_2: "",
      mutation_3: "",
      mut_0: 0,
      mut_1: 0,
      mut_2: 0,
      mut_3: 0,
      mut_4: 0,
      mut_5: 0,
      mut_6: 0,
      mut_7: 0,
      mut_8: 0,
      mut_9: 0,
      rot_0: 0,
      rot_1: 0,
      rot_2: 0,
      rot_3: 0,
      rot_4: 0,
      rot_5: 0,
      rot_6: 0,
      rot_7: 0,
      rot_8: 0,
      rot_9: 0,
      armor: "",
      armor_val: 0,
      occupation: "",
      tal_0: "",
      tal_1: "",
      tal_2: "",
      tal_3: "",
      tal_4: "",
      gear_0: "",
      gear_1: "",
      gear_2: "",
      gear_3: "",
      gear_4: "",
      gear_5: "",
      gear_6: "",
      gear_7: "",
      gear_8: "",
      gear_9: "",
      ammo: 0,
      wpn_0: "",
      wpn_0_1: 0,
      wpn_0_2: 0,
      wpn_0_3: "",
      wpn_0_4: "",
      wpn_1: "",
      wpn_1_1: 0,
      wpn_1_2: 0,
      wpn_1_3: "",
      wpn_1_4: "",
      wpn_2: "",
      wpn_2_1: 0,
      wpn_2_2: 0,
      wpn_2_3: "",
      wpn_2_4: "",
      rel_0: "",
      rel_0_1: 0,
      rel_1: "",
      rel_1_1: 0,
      rel_2: "",
      rel_2_1: 0,
      rel_3: "",
      rel_3_1: 0,
      rel_4: "",
      rel_5: "",
      rel_6: "",
    };
  }
  $scope.initForm();
});
