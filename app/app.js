var app = angular.module('myApp', ['angular-toArrayFilter', 'ngRoute', 'appControllers', 'toaster']);

app.config(['$routeProvider',
  function ($routeProvider) {
        $routeProvider.
        when('/', {
            title: 'Home',
            templateUrl: 'partials/home.html',
            controller: 'indexCtrl',
            role: '0'
        })
        .otherwise({
            redirectTo: '/home'
        });
  }])
  // .run(function ($rootScope, $location, Data) {
  //     $rootScope.$on("$routeChangeStart", function (event, next, current) {
  //         $rootScope.authenticated = false;
  //         Data.get('session').then(function (results) {
  //             if (results.uid) {
  //                 $rootScope.authenticated = true;
  //                 $rootScope.uid = results.uid;
  //                 $rootScope.name = results.name;
  //                 $rootScope.email = results.email;
  //             } else {
  //                 var nextUrl = next.$$route.originalPath;
  //                 if (nextUrl != '/dashboard' ) {

  //                 } else {
  //                     $location.path("/login");
  //                 }
  //             }
  //         });
  //     });
  // });