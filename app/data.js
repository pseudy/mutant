app.factory("Data", ['$http', 'toaster',
    function ($http, toaster) { // This service connects to our REST API
      var serviceBase;
      if(location.hostname === "www.eternivision.com") {
          serviceBase = 'api/index.php/';
      } else {
          // use api on server if on localhost
          serviceBase = 'http://www.eternivision.com/api/index.php/';
      }

      var obj = {};
      obj.toast = function (data) {
        if(data && !data.message) {
          data.message = data;
        }
          toaster.pop(data.status, "", data.message, 10000, 'trustedHtml');
      };
      obj.get = function (q) {
          return $http.get(serviceBase + q).then(function (results) {
              return results.data;
          });
      };
      obj.post = function (q, object) {
          return $http.post(serviceBase + q, object).then(function (results) {
              return results.data;
          });
      };
      obj.put = function (q, object) {
          return $http.put(serviceBase + q, object).then(function (results) {
              return results.data;
          });
      };
      obj.delete = function (q) {
          return $http.delete(serviceBase + q).then(function (results) {
              return results.data;
          });
      };

      return obj;
}]);